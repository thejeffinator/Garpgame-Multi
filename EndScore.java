import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;
import java.util.List;

/**
 * Write a description of class EndScore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EndScore extends Score
{
    private long startTime;
    
    
    /**
     * Act - do whatever the EndScore wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } 
    public EndScore(){
        startTime = System.currentTimeMillis();
        
    }
        public String getElapsedTime(){
        long duration;
        int uren, minuten, seconden;
        String duur;
        
        
        
        duration = System.currentTimeMillis() - startTime;
        
        seconden = (int)(duration/1000);
        uren = seconden / 3600;
        seconden %= 3600;
        minuten = (int) (seconden/60);
        seconden %= 60;
        duur = String.format("Duur:%02d:%02d:%02d", uren, minuten, seconden);
        
        return duur;
    }
    public void setEndImage(int score, int score2 ){
        GreenfootImage image;
        Dimension dim;
        String duur, resultaat, punten,punten2;
        Counter counter;
        World world;
        List lijst;
        int fontSize  = 24;
        int breedte = 700;
        int hoogte = 300;
        
        
        world = getWorld();
        lijst = world.getObjects(Counter.class);
        counter = (Counter)lijst.get(0);
        
        resultaat ="Game Over!";
        
        score = counter.getScore();
        score2 = counter.getScore2();
  
        if(score == 20){
            resultaat ="Mario heeft alle Coins verzameld!";
            Greenfoot.playSound("Mariowin.wav");
            }
        
        if(score2 == 20){
                resultaat ="Link heeft alle rupee's verzameld!";
                Greenfoot.playSound("Linkwin.wav");
            }    
        

        image = new GreenfootImage(breedte,hoogte);
        punten =("Mario heeft "+score+" Coin's gevangen !");
        punten2 =("Link heeft "+score2+" Rupee's gevangen !");
        
        duur = getElapsedTime();
        Font font = image.getFont();
        font = font.deriveFont(0,35);
        image.setFont(font);
        dim = getTextDimensions(image,punten+20);
        image.scale(dim.width,dim.height*5);
        
        setImage(image);
        image.setColor(Color.red);
        image.fillRect(0,0,breedte,hoogte);
        image.setColor(Color.white);
       
        image.drawString(resultaat,5,dim.height);
        image.drawString(duur,5,dim.height+85);
        image.drawString(punten,5,dim.height+125);
        image.drawString(punten2,5,dim.height+165);

        
       
        
    }
    
}