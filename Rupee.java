import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Rupee here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rupee extends Actor
{
    /**
     * Act - do whatever the Rupee wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
    followLink();

    lookforBomb();
 
    }  
    
    public void followLink(){
     String target;
     List<Link> group = getObjectsInRange(50,Link.class);
     if(group.size()>0){
         Actor Link = group.get(0);
         int targetX = Link.getX();
         int targetY = Link.getY();
         turnTowards(targetX, targetY);
        }
    }
 
      protected void lookforRock(){
         int x,y;
         Actor Rock;
     
         Rock = getOneObjectAtOffset(0,0,Rock.class);
         if((Rock != null)){
            x = getX()+10;
            y = getY()+10;
            setLocation(x,y);
        }
 }
 public void lookforBomb(){
        int x,y;
        Actor Bomb;
        
        Bomb = getOneObjectAtOffset(0,0,Bomb.class);
        if(Bomb!= null){
            x = getX()+10;
            y = getY()+10;
            setLocation(x,y);
        }
    }
}
