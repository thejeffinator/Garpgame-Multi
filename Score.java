import greenfoot.*;
import greenfoot.GreenfootImage;
import greenfoot.Actor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Dimension;  

/**
 * Write a description of class Score here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Score extends Actor
{

    /**
     * Act - do whatever the Score wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Dimension getTextDimensions(GreenfootImage image, String tekst) 
    {
       int height, width;
       Font font;
       font=image.getFont();
       FontMetrics metrics = image.getAwtImage().getGraphics().getFontMetrics(font);
       width= metrics.stringWidth(tekst);
       height = metrics.getLeading() + metrics.getAscent()+metrics.getDescent();
       return new Dimension(width,height);
    }    
}
