import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Mario here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Mario extends Actor
{
    private GreenfootImage imageLeft;
    private GreenfootImage imageRight;
    

    public Mario()
    {
        imageLeft = new GreenfootImage("Marioleft.png");
        imageRight = new GreenfootImage("Marioright.png");
        setImage (imageRight);
        
        
    }
    public void act() 
    { 
        move();
        collectingCoins();
        foundBomb();
        
    }
    
    public void collectingCoins(){
     World world;
     Actor Coin;
     Counter counter;
     List lijst;
    
     Coin=getOneObjectAtOffset(0,0,Coin.class);
     if(Coin!= null){
        world=getWorld();
        Greenfoot.playSound("Coin.wav");
        lijst = world.getObjects(Counter.class);
        counter = (Counter)lijst.get(0);
        counter.addScore();
        world.removeObject(Coin);
         if(counter.getScore() == 21){
         Greenfoot.stop();
        }
     }
     

   }
    protected boolean foundRock(){
        Actor Rock;
        Rock=getOneObjectAtOffset(0,0,Rock.class);
        if(Rock!= null){
        return true;
      }
      return false;
    }
    
    public void foundBomb(){
        Actor Bomb;
        getX(); getY();
        Bomb =getOneObjectAtOffset(0,0,Bomb.class);
        if(Bomb != null){
            getWorld().removeObject(Bomb);
            getWorld().addObject(new Explosion(), getX(),getY());
            getWorld().removeObject(this);
            
        }
    }
    public void move()
    {
        if(Greenfoot.isKeyDown("D")){
           if(getImage() == imageLeft){
               setImage(imageRight);
            }
           setRotation(0);
           move(5);
        }
        
        if(Greenfoot.isKeyDown("A")){
           if(getImage() == imageRight){
               setImage(imageLeft);
            }
           setRotation(0);
           move(-5);
            if(foundRock()){
            move (5);
        }
       }
             
        
        if(Greenfoot.isKeyDown("W")){
           setImage(imageRight); 
           setRotation(-90);
           move(5);
        }
        
        if(Greenfoot.isKeyDown("S")){
           setImage(imageRight);
           setRotation(90);
           move(5);
        }
        
        if(foundRock()){
            move (-5);
       }
       

   }
}