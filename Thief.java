import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Thief here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Thief extends Actor
{
 
    /**
     * Act - do whatever the Thief wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    

    public void act() 
    {
        lookforBomb();
        lookforRock();
        lookforMario();
        lookforLink();
        followMario();
        followLink();

       
        
       move(3);
        if (Greenfoot.getRandomNumber(100)<10){
        turn(Greenfoot.getRandomNumber(90)-45);
    }
    if (getX() <= 5 || getX()>= getWorld().getWidth() -5)
    {
        turn(180);
    }
    if (getY() <= 5 || getY() >= getWorld().getHeight()- 5)
    {
        turn(180);
    }

 }
 
 protected void lookforBomb(){
     Actor Bomb;
     
     Bomb = getOneObjectAtOffset(0,0,Bomb.class);
     if((Bomb != null)){
     turn (180);
    }
 }
 protected void lookforRock(){
     Actor Rock;
     
     Rock = getOneObjectAtOffset(0,0,Rock.class);
     if((Rock != null)){
     turn (180);
    }
 }
    protected void lookforMario(){
    Actor Mario; 
        
    Mario = getOneObjectAtOffset(0,0,Mario.class);
    if(Mario != null){
     getWorld().removeObject(Mario);
     
     
     //Greenfoot.stop();
     Greenfoot.playSound("Mariodead.wav");
    }
 }
 
    protected void lookforLink(){
    Actor Link;
    
    Link = getOneObjectAtOffset(0,0,Link.class);
    if(Link != null){

        getWorld().removeObject(Link);
        
       
       //Greenfoot.stop();
       Greenfoot.playSound("Linkdead.wav");
    }
   }
 
     public void followMario(){
     String target;
     List<Mario> group = getObjectsInRange(150,Mario.class);
     if(group.size()>0){
         Actor Mario = group.get(0);
         int targetX = Mario.getX();
         int targetY = Mario.getY();
         turnTowards(targetX, targetY);
        }
    }
    
      public void followLink(){
     String target;
     List<Link> group = getObjectsInRange(150,Link.class);
     if(group.size()>0){
         Actor Link = group.get(0);
         int targetX = Link.getX();
         int targetY = Link.getY();
         turnTowards(targetX, targetY);
        }
    }
}
