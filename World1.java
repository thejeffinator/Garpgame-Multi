import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Write a description of class GarpWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class World1 extends World
{
    private GreenfootSound sound,sound1;
    private EndScore endScore;
    private Counter counter;

    
    public World1(){    
        super(1100, 600, 1); 
        populateTheWorld();

        prepare();
    }

    protected void populateTheWorld()
    {
        int teller, regel, kolom;

        sound1 = new GreenfootSound("Let's A Go.wav");

        counter = new Counter();
        regel = getHeight() - counter.getImage().getHeight()/2-1;
        kolom = counter.getImage().getWidth()/2+1;

        addObject(new Mario(),50,550);
        addObject(new Link(),900,550);
        addObject(new Counter(),kolom,regel);  
        addObject(new Thief(), 500,20);

        for(teller =0; teller <20; teller ++){
            addObject(new Rupee(),Greenfoot.getRandomNumber(1050)+20,Greenfoot.getRandomNumber(500)+20);
        }

        for(teller =0; teller <20; teller ++){
            addObject(new Coin(),Greenfoot.getRandomNumber(1050)+20,Greenfoot.getRandomNumber(500)+20);
        }

        for(teller =0; teller <10; teller ++){
            addObject(new Bomb(),Greenfoot.getRandomNumber(1060)+20,Greenfoot.getRandomNumber(500)+30);
        }

        setPaintOrder(EndScore.class, Counter.class,Mario.class, Thief.class,Bomb.class, Rock.class, Rupee.class,Coin.class );
        sound = new GreenfootSound("Mario Theme.mp3");
    }
    public void started()
    {
        endScore = new EndScore();

        sound.playLoop();
    }

    public void stopped()
    {
        int score,score2;
        addObject(endScore, getWidth()/2, getHeight()/2);
        score = counter.getScore();
        score2 = counter.getScore2();
        endScore.setEndImage(score,score2);

            
        sound.stop();

    }

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare() //prepare rock
    {
        Rock rock = new Rock();
        addObject(rock, 968, 70);
        Rock rock2 = new Rock();
        addObject(rock2, 1047, 208);
        Rock rock3 = new Rock();
        addObject(rock3, 846, 206);
        Rock rock4 = new Rock();
        addObject(rock4, 758, 55);
        Rock rock5 = new Rock();
        addObject(rock5, 72, 62);
        Rock rock6 = new Rock();
        addObject(rock6, 206, 169);
        Rock rock7 = new Rock();
        addObject(rock7, 353, 85);
        Rock rock8 = new Rock();
        addObject(rock8, 519, 145);
        Rock rock9 = new Rock();
        addObject(rock9, 676, 92);
        Rock rock10 = new Rock();
        addObject(rock10, 696, 246);
        Rock rock11 = new Rock();
        addObject(rock11, 489, 343);
        Rock rock12 = new Rock();
        addObject(rock12, 309, 263);
        Rock rock13 = new Rock();
        addObject(rock13, 506, 236);
        Rock rock14 = new Rock();
        addObject(rock14, 666, 331);
        Rock rock15 = new Rock();
        addObject(rock15, 926, 475);
        Rock rock16 = new Rock();
        addObject(rock16, 1033, 499);
        Rock rock17 = new Rock();
        addObject(rock17, 747, 597);
        Rock rock18 = new Rock();
        addObject(rock18, 475, 538);
        Rock rock19 = new Rock();
        addObject(rock19, 239, 547);
        Rock rock20 = new Rock();
        addObject(rock20, 374, 479);
        Rock rock21 = new Rock();
        addObject(rock21, 619, 477);
        Rock rock22 = new Rock();
        addObject(rock22, 794, 459);
        Rock rock23 = new Rock();
        addObject(rock23, 838, 407);
        Rock rock24 = new Rock();
        addObject(rock24, 1031, 342);
        Rock rock25 = new Rock();
        addObject(rock25, 976, 277);
        Rock rock26 = new Rock();
        addObject(rock26, 62, 325);
        Rock rock27 = new Rock();
        addObject(rock27, 265, 310);
        Rock rock28 = new Rock();
        addObject(rock28, 115, 456);
        Rock rock29 = new Rock();
        addObject(rock29, 171, 409);
        Rock rock30 = new Rock();
        addObject(rock30, 97, 549);
        Rock rock31 = new Rock();
        addObject(rock31, 51, 462);
        Rock rock32 = new Rock();
        addObject(rock32, 522, 471);
        Rock rock33 = new Rock();
        addObject(rock33, 610, 573);
        Rock rock34 = new Rock();
        addObject(rock34, 708, 414);
        Rock rock35 = new Rock();
        addObject(rock35, 404, 279);
        Rock rock36 = new Rock();
        addObject(rock36, 262, 40);
        Rock rock37 = new Rock();
        addObject(rock37, 891, 44);
        Rock rock38 = new Rock();
        addObject(rock38, 959, 203);
        Rock rock39 = new Rock();
        addObject(rock39, 358, 186);
        Rock rock40 = new Rock();
        addObject(rock40, 39, 242);
        Rock rock41 = new Rock();
        addObject(rock41, 617, 406);
        Rock rock42 = new Rock();
        addObject(rock42, 337, 378);
        Rock rock43 = new Rock();
        addObject(rock43, 585, 302);
        Rock rock44 = new Rock();
        addObject(rock44, 343, 571);
    }
}

