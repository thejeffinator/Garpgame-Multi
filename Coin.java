import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Coin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Coin extends Actor
{
    /**
     * Act - do whatever the Coin wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
    followMario();
    lookforRock();
    lookforBomb();

    }  

    
    public void followMario(){
     String target;
     List<Mario> group = getObjectsInRange(50,Mario.class);
     if(group.size()>0){
         Actor Mario = group.get(0);
         int targetX = Mario.getX();
         int targetY = Mario.getY();
         turnTowards(targetX, targetY);
        }
    }
    
     protected void lookforRock(){
         int x,y;
         Actor Rock;
     
         Rock = getOneObjectAtOffset(0,0,Rock.class);
         if((Rock != null)){
            x = getX()+10;
            y = getY()+10;
            setLocation(x,y);
        }
 }
 
 public void lookforBomb(){
        int x,y;
        
        Actor Bomb;
        
        Bomb = getOneObjectAtOffset(0,0,Bomb.class);
        if(Bomb!= null){
            x = getX()+10;
            y = getY()+10;
            setLocation(x,y);
        }
    }
}
