import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bomb here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bomb extends Actor
{
    /**
     * Act - do whatever the Bomb wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        
    }    
    
    public void LookforRock(){
        int x,y;
        
        Actor rock;
        
        rock = getOneObjectAtOffset(0,0,Rock.class);
        if(rock!= null){
            x = getX()+10;
            y = getY()+10;
            setLocation(x,y);
        }
    }
}
