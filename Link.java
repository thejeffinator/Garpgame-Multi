import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Link here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Link extends Actor
{
    private GreenfootImage imageLeft;
    private GreenfootImage imageRight;

    public void act() 
    {
        move();
        collectingRupees();
        foundBomb();
        
    }
    public Link(){
        imageLeft = new GreenfootImage("linkleft.png");
        imageRight = new GreenfootImage("linkright.png");
        setImage (imageRight);
    }
    public void move(){
    if(Greenfoot.isKeyDown("right")){
            if(getImage() == imageLeft){
               setImage(imageRight);
            }
           setRotation(0);
           move(5);
        }
        
        if(Greenfoot.isKeyDown("left")){
          if(getImage() == imageRight){
               setImage(imageLeft);
            }
           setRotation(0);
           move(-5);
            if(foundRock()){
            move (5);
        }
    }
        
        if(Greenfoot.isKeyDown("up")){ 
           setImage(imageRight);
           setRotation(-90);
           move(5);
        }
        
        if(Greenfoot.isKeyDown("down")){
           setImage(imageRight);
           setRotation(90);
           move(5);
        }
        
        if(foundRock()){
            move (-5);
       }
    }
     protected boolean foundRock(){
        Actor Rock;
        Rock=getOneObjectAtOffset(0,0,Rock.class);
        if(Rock!= null){
        return true;
      }
      return false;
    }
    public void collectingRupees(){
     World world;
     Actor Rupee;
     Counter counter;
     List lijst;
    
     Rupee=getOneObjectAtOffset(0,0,Rupee.class);
     if(Rupee!= null){
        world=getWorld();
        Greenfoot.playSound("Coin.wav");
        lijst = world.getObjects(Counter.class);
        counter = (Counter)lijst.get(0);
        counter.addScore2();
        world.removeObject(Rupee);
         if(counter.getScore2() == 20){
         Greenfoot.stop();
        }
     }
     
    }
    public void foundBomb(){
        Actor Bomb;
        getX(); getY();
        Bomb =getOneObjectAtOffset(0,0,Bomb.class);
        if(Bomb != null){
            getWorld().removeObject(Bomb);
            getWorld().addObject(new Explosion(), getX(),getY());
            getWorld().removeObject(this);
            
        }
    }
}
