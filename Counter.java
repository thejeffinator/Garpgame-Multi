import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;


/**
 * Write a description of class Counter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Counter extends Score
{
    private int score;
    private int score2;
    
        public int getScore(){
        return score;
    }
        public int getScore2(){
        return score2;
    }
    
    public Counter(){
        String scoreText,scoreText2;
        GreenfootImage image;
        Dimension dim;
    
        
        image = new GreenfootImage(1100,37);

        
        score = 0;
        score2 = 0;
        
        scoreText= "Mario heeft "+ score + " coins";
        scoreText2= "Link heeft "+ score2 + " rupee's";
        
        
        dim = getTextDimensions(image, scoreText);
        
        
        setImage(image);
        image.setColor(Color.white);
        image.drawString(scoreText, 2, dim.height);
        image.drawString(scoreText2, 950, dim.height);
        

    }
    
        public void addScore(){
        score++;
        updateImage();
    }
        public void addScore2()
    {  score2 ++;
        updateImage();
    }
        public void updateImage(){
        String scoreText,scoreText2;
        Dimension dim;
        GreenfootImage image =getImage();
        
        
        image.clear();
        scoreText = "Aantal Coins Mario: "+ score;
        scoreText2 = "Aantal Rupee's Link: "+ score2;
        
        dim= getTextDimensions(image, scoreText);
        //setImage(image);
        
        image.setColor(Color.white);
        image.drawString(scoreText, 2, dim.height);
        image.drawString(scoreText2, 950, dim.height);
        
    }
    

}